## Technologies used for the project
- [Vite.js](https://vitejs.dev/guide/)
- [Typescript](https://www.typescriptlang.org/)
- [TailwindCSS](https://tailwindcss.com/docs/guides/vite)
- [Shadcn/ui](https://ui.shadcn.com/docs)
- [Flowbite](https://flowbite.com/)

## To get started
```bash
pnpm install
pnpm run dev || pnpm dev
```

__Well that's it, explore the code with dev server runnning__