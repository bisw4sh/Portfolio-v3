import { ImRocket } from "react-icons/im";

const ToTop = () => {

  return (
    <div className="fixed bottom-4 right-4 h-7 animate-bounce">
      <ImRocket
        onClick={() => window.scrollTo(0, 0)}
        className="h-full w-full hover:fill-fuchsia-600 rotate-[-46deg]"
      />
    </div>
  );
};

export default ToTop;