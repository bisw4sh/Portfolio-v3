import { FaFolder } from "react-icons/fa";
import { BsStack } from "react-icons/bs";
import { ImGithub } from "react-icons/im";
import { FaArrowLeft } from "react-icons/fa";

import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";

const ProjectCard = ({title, desc} : {
    title : string,
    desc: string
}) => {
  return (
    <div id='projects' className="relative w-full flex m-2 p-3 max-w-[24rem] flex-col rounded-xl outline text-purple-600 bg-clip-border shadow-md">
      <div className="relative px-8 flex justify-around m-0 overflow-hidden text-gray-700 bg-transparent rounded-none shadow-none bg-clip-border">
        <AlertDialog>
          <AlertDialogTrigger>
            {/* Here */}
            <FaFolder className="text-xl hover:fill-zinc-500" />
          </AlertDialogTrigger>
          <AlertDialogContent>
            <AlertDialogHeader>
              <AlertDialogTitle>Unavailable</AlertDialogTitle>
              <AlertDialogDescription>
                Maybe, it is either removed or is private
              </AlertDialogDescription>
            </AlertDialogHeader>
            <AlertDialogFooter>
              <AlertDialogCancel>
                <FaArrowLeft />
                {/* Fair enough */}
              </AlertDialogCancel>
              <AlertDialogAction>Ok! Cool</AlertDialogAction>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialog>

        <AlertDialog>
          <AlertDialogTrigger>
            {/* Here */}
            <BsStack className="text-4xl fill-zinc-800 hover:fill-zinc-500" />
          </AlertDialogTrigger>
          <AlertDialogContent>
            <AlertDialogHeader>
              <AlertDialogTitle>Unavailable</AlertDialogTitle>
              <AlertDialogDescription>
                Maybe, it is either removed or is private
              </AlertDialogDescription>
            </AlertDialogHeader>
            <AlertDialogFooter>
              <AlertDialogCancel>
                <FaArrowLeft />
                {/* Fair enough */}
              </AlertDialogCancel>
              <AlertDialogAction>Ok! Cool</AlertDialogAction>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialog>

        <a href="https://github.com/bisw4sh">
          <ImGithub className="text-xl hover:fill-purple-800" />
        </a>
      </div>
      <div className="p-3">
        <h4 className="flex justify-start max-md:justify-center font-sans text-2xl antialiased font-semibold leading-snug tracking-normal text-blue-gray-900">
          {/* UI/UX Review Check */}
          {title}
        </h4>
        <p className="block font-sans text-xl antialiased font-normal text-gray-700 max-md:text-center max-sm:hidden w-full">
          {/* Because it's about motivating the doers. Because I'm here to follow my
          dreams and inspire others. */}
          {desc}
        </p>
      </div>
    </div>
  );
};

export default ProjectCard;
