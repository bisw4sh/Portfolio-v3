const Stepper = () => {
  return (
    <ol className="relative border-s border-gray-200 dark:border-gray-700 mt-8">
      <li className="mb-10 ms-4">
        <div className="absolute w-3 h-3 bg-gray-200 rounded-full mt-1.5 -start-1.5 border border-white dark:border-gray-900 dark:bg-gray-700"></div>
        <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-500">
          Completed on 2072 (15-16)
        </time>
        <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
          School Leaving Certificate
        </h3>
        <p className="mb-4 text-base font-normal text-gray-500 dark:text-gray-400">
          Completed the grade 10 of education from Diamond Secondary School
        </p>
      </li>
      <li className="mb-10 ms-4">
        <div className="absolute w-3 h-3 bg-gray-200 rounded-full mt-1.5 -start-1.5 border border-white dark:border-gray-900 dark:bg-gray-700"></div>
        <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-500">
          Completed on 2072-73 (16-18)
        </time>
        <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
          National Education Board
        </h3>
        <p className="text-base font-normal text-gray-500 dark:text-gray-400">
          Completed the Science stream education from Amarsingh Secondary School
        </p>
      </li>
      <li className="ms-4">
        <div className="absolute w-3 h-3 bg-gray-200 rounded-full mt-1.5 -start-1.5 border border-white dark:border-gray-900 dark:bg-gray-700"></div>
        <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-500">
          Started from 2019 - 4 yr course
        </time>
        <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
          Degree of Bachelor in Software Engineering
        </h3>
        <p className="text-base font-normal text-gray-500 dark:text-gray-400">
            Took a course of BSE in Pokhara University
        </p>
      </li>
    </ol>
  );
}

export default Stepper