import mainImg from '../../images/main.png'
import Code from "../../svgs/1.svg";
import down from "../../svgs/down.svg";
import { useToast } from "@/components/ui/use-toast";
import { Button } from "@/components/ui/button";
import {
  HoverCard,
  HoverCardContent,
  HoverCardTrigger,
} from "@/components/ui/hover-card";
// import resume from "../../resume/resume.pdf";

const About = () => {

    const borderRadiusStyle: React.CSSProperties = {
      borderRadius: "25% 62% 0% 90% / 0% 70% 25% 100%",
    };

    const { toast } = useToast();

  return (
    <div id='about' className="relative flex md:flex-row flex-col-reverse max-lg:mt-2 lg:mt-16 md:px-24 justify-between items-center ">
      {/* Summary Section */}
      <div className="font-bold max-md:space-y-4 lg:space-y-6 max-lg:mt-8 lg:mt-16">
        <h1 className="text-5xl">
          Hi,{" "}
          {/* <span className="text-transparent text-base bg-clip-text bg-gradient-to-r from-purple-800 to-pink-800">
            <a
              href={resume}
              download="Biswash Dhungana"
              target="_blank"
              rel="noreferrer"
            >
            </a>
          </span> */}
        </h1>
        <h2 className="text-3xl">
          I'm{" "}
          <span className="text-transparent text-3xl bg-clip-text bg-gradient-to-r from-purple-800 to-pink-800">
            Biswash Dhungana
          </span>
        </h2>
        <h3 className="text-3xl cursor-wait">
          <HoverCard>
            <HoverCardTrigger> Web Developer</HoverCardTrigger>
            <HoverCardContent className="font-semibold">
              on my way - 🛴
            </HoverCardContent>
          </HoverCard>
        </h3>
        <h4 className="text-3xl">from Pokhara, Nepal</h4>
        <Button
          onClick={() => {
            toast({
              title: "Put more power behind it",
              description:
                "Well, forget to mention. Either your should be my friend or someone with shit ton of cash",
            });
          }}
        >
          <span className="font-semibold text-base">Summon me</span>
        </Button>
        <div className="h-16 py-16 mt-32 max-md:hidden">
          <img
            src={down}
            alt="down"
            className="opacity-60 py-8 ml-8 animate-bounce"
          />
        </div>
      </div>

      <div className="absolute margin-auto top-32 left-8 max-md:hidden -z-10">
        <img
          src={Code}
          alt="Code"
          className="opacity-40 animate-pulse animate-infinite h-64"
        />
      </div>

      <div
        className="relative h-full w-full md:w-1/3"
        style={borderRadiusStyle}
      >
        <img
          src={mainImg}
          alt="biswash's image"
          className="h-fit drop-shadow-2xl rounded-full"
        />

        {/* <div
          className="absolute top-0 rounded-lg opacity-30 h-full w-full bg-gradient-to-r from-cyan-500 to-blue-500"
          // style={borderRadiusStyle}
        ></div> */}
      </div>
    </div>
  );
}

export default About