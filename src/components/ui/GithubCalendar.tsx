import { useState, useEffect } from "react";
import GitHubCalendar from "react-github-calendar";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";


export default function GithubCalendar() {
  type ColorScheme = "dark" | "light" | undefined;
  const [colorScheme, setColorScheme] = useState<ColorScheme>();

  useEffect(() => {
    const storedTheme = localStorage.getItem("vite-ui-theme");

    const handleStorageChange = () => {
      setColorScheme( storedTheme === "light" || storedTheme === "dark" ? storedTheme : undefined )
    }

    window.addEventListener("storage", handleStorageChange)

    handleStorageChange()

    return () => {
      window.removeEventListener("storage", handleStorageChange)
    }
  }, [])

  return (
    <div className="w-full flex flex-col gap-5 justify-center items-center pt-8 px-4 text-purple-600">
      <TooltipProvider>
        <Tooltip>
          <TooltipTrigger>Github Activity</TooltipTrigger>
          <TooltipContent>
            <p className="text-xl font-semibold">
              If there are less green, well they're hidden
            </p>
          </TooltipContent>
        </Tooltip>
      </TooltipProvider>

      {/* <h1 className="text-xl font-semibold">Github Activity</h1> */}
      <GitHubCalendar
        username="bisw4sh"
        blockSize={15}
        blockMargin={7}
        fontSize={16}
        colorScheme={colorScheme as ColorScheme}
      />
    </div>
  );
}
