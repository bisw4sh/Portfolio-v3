import CForm from './CForm';

const Contact = () => {
  return (
    <>
      <div id='contacts' className="max-lg:px-8 lg:px-24 max-lg:mt-8 lg:mt-16">
        <h1 className="font-roboto font-black	text-transparent text-6xl bg-clip-text bg-gradient-to-r from-purple-800 to-pink-800">
          Contact
        </h1>
        <CForm />
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#f3f4f5"
          fill-opacity="0.7"
          d="M0,128L130.9,64L261.8,64L392.7,0L523.6,32L654.5,64L785.5,288L916.4,32L1047.3,128L1178.2,0L1309.1,32L1440,224L1440,320L1309.1,320L1178.2,320L1047.3,320L916.4,320L785.5,320L654.5,320L523.6,320L392.7,320L261.8,320L130.9,320L0,320Z"
        ></path>
      </svg>
    </>
  );
}

export default Contact