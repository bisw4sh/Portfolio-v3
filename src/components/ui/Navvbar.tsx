import { ModeToggle } from "../mode-toggle";
import NavList from "./NavList";
import { Button } from "@/components/ui/button";
import {
  HoverCard,
  HoverCardContent,
  HoverCardTrigger,
} from "@/components/ui/hover-card";
import resume from '../../resume/resume.pdf'

const Navvbar = () => {
  return (
    <div className="mt-4 max-sm:px-2 max-md:px-4 md:px-8 w-full flex justify-between items-center">
      <span
        className="max-sm:hidden font-bold text-3xl cursor-text text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600
      animate-pulse animate-infinite animate-ease-in-out"
      >
        विश्वाश
      </span>

      <span
        className="sm:hidden font-bold text-3xl cursor-text text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600
      animate-pulse animate-infinite animate-ease-in-out"
      >
        विश्वाश
      </span>

      <NavList />

      <div className="flex gap-4 max-md:hidden">
        <HoverCard>
          <HoverCardTrigger>
            {" "}
            <Button>
              <a
                href={resume}
                download="Biswash Dhungana"
                target="_blank"
                rel="noreferrer"
              >
                Résumé
              </a>
            </Button>
          </HoverCardTrigger>
          <HoverCardContent>
            There shall be a pdf once constructed
          </HoverCardContent>
        </HoverCard>

        <ModeToggle />
      </div>
    </div>
  );
};

export default Navvbar;