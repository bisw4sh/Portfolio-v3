import { Link } from "react-router-dom";
import { ModeToggle } from "../mode-toggle";
import { GiHamburgerMenu } from "react-icons/gi";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import resume from "../../resume/resume.pdf";

const NavList = () => {
  return (
    <div>
      <ul className="max-md:hidden flex items-center gap-5 capitalize font-medium">
        {["about", "skills", "projects", "contacts"].map((item) => {
          const url = `#${item}`;
          return (
            <li
              key={item}
              className="cursor-pointer text-lg hover:underline decoration-wavy	decoration-2 underline-offset-8"
            >
              <a href={url}>{item}</a>
            </li>
          );
        })}
        <li
          key="hashnode"
          className="cursor-pointer text-lg hover:underline decoration-wavy	decoration-2 underline-offset-8"
        >
          <a href="https://blogs.biswashdhungana.com.np/">Blogs</a>
        </li>
      </ul>

      <ul className="md:hidden flex gap-3 text-3xl hover:cursor-pointer">
        <ModeToggle />
        <DropdownMenu>
          <DropdownMenuTrigger>
            <GiHamburgerMenu />
          </DropdownMenuTrigger>
          <DropdownMenuContent>
            <DropdownMenuLabel>Scroll To</DropdownMenuLabel>
            <DropdownMenuSeparator />
            <DropdownMenuItem>
              <Link to="#about">About</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <Link to="#skills">Skills</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <Link to="#projects">Projects</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <Link to="#contacts">Contact</Link>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <a href="https://blogs.biswashdhungana.com.np/">Blogs</a>
            </DropdownMenuItem>
            <DropdownMenuItem>
              <a
                href={resume}
                download="Biswash Dhungana"
                target="_blank"
                rel="noreferrer"
              >
                Résumé
              </a>
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
      </ul>
    </div>
  );
};

export default NavList;
