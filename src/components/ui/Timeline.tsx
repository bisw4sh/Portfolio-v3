import Stepper from "./Stepper";

const Timeline = () => {
  return (
    <div className="lg:px-24 max-lg:px-8 mt-16 ">
      <h1 className="font-roboto font-black	text-transparent text-6xl bg-clip-text bg-gradient-to-r from-purple-800 to-pink-800">
        Career
      </h1>
      <Stepper />
    </div>
  );
}

export default Timeline