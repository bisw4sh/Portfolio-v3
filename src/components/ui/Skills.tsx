import { IoPricetags } from "react-icons/io5";
import { FaReact } from "react-icons/fa";
import { IoLogoJavascript, IoLogoNodejs } from "react-icons/io5";
import { SiTypescript, SiTailwindcss } from "react-icons/si";
import { TbBrandNextjs } from "react-icons/tb";
import { FaDatabase, FaGolang } from "react-icons/fa6";
import { LuListTodo } from "react-icons/lu";
import ProjectCard from "./ProjectCard";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";

const Skills = () => {
  return (
    <div id='skills' className="lg:px-24 md:px-8 max-md:px-8 mt-32 max-md:mt-16">
      <h1 className="font-roboto font-black	text-transparent text-6xl bg-clip-text bg-gradient-to-r from-purple-800 to-pink-800">
        <span className="mr-3">Skills</span>
        <AlertDialog>
          <AlertDialogTrigger>
            {/* Here */}
            <IoPricetags className="text-5xl font-bold fill-purple-800 hover:fill-pink-800 rotate-[250deg] max-sm:hidden" />
          </AlertDialogTrigger>
          <AlertDialogContent>
            <AlertDialogHeader>
              <AlertDialogTitle>Honestly, speaking</AlertDialogTitle>
              <AlertDialogDescription>
                I've no idea how capable I am, but what I know is I get the work
                done.
              </AlertDialogDescription>
            </AlertDialogHeader>
            <AlertDialogFooter>
              <AlertDialogCancel>Fair enough</AlertDialogCancel>
              <AlertDialogAction>Ok! Cool</AlertDialogAction>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialog>
      </h1>

      <div className="flex max-md:flex-col  gap-8">
        <div className="w-fit mt-8 max-md:w-full">
          <ul className="flex flex-col justify-between gap-3">
            {[
              ["javascript", IoLogoJavascript],
              ["typescript", SiTypescript],
              ["tailwindCSS", SiTailwindcss],
              ["react.js", FaReact],
              ["node.js", IoLogoNodejs],
              ["Go", FaGolang],
              ["next.js", TbBrandNextjs],
            ].map(([tech, TechIcon], idx) => {
              return (
                <li
                  key={idx}
                  className="flex max-md:w-full text-3xl gap-3 items-center outline rounded px-2 py-2 capitalize text-purple-400 cursor-pointer hover:text-purple-900"
                >
                  <TechIcon />
                  <span className="text-transparent bg-clip-text bg-gradient-to-r from-purple-400 to-purple-800">
                    {tech}
                  </span>
                </li>
              );
            })}

            <DropdownMenu>
              <DropdownMenuTrigger>
                <div className="flex max-md:w-full text-3xl gap-3 items-center outline rounded px-3 py-2 capitalize text-purple-400 cursor-pointer hover:text-purple-900">
                  <FaDatabase />
                  <span>Databases</span>
                </div>
              </DropdownMenuTrigger>
              <DropdownMenuContent>
                <DropdownMenuLabel>Databases that I've used</DropdownMenuLabel>
                <DropdownMenuSeparator />
                <DropdownMenuItem>MongoDB</DropdownMenuItem>
                <DropdownMenuItem>PostgreSQL</DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>

            <DropdownMenu>
              <DropdownMenuTrigger>
                <div className="flex max-md:w-full text-3xl gap-3 items-center outline rounded px-3 py-2 capitalize text-purple-400 cursor-pointer hover:text-purple-900">
                  <LuListTodo />
                  <span>Further</span>
                </div>
              </DropdownMenuTrigger>
              <DropdownMenuContent>
                <DropdownMenuLabel>
                  Technologies I'm planning learning next
                </DropdownMenuLabel>
                <DropdownMenuSeparator />
                <DropdownMenuItem>GraphQL</DropdownMenuItem>
                <DropdownMenuItem>Prisma ORM</DropdownMenuItem>
                <DropdownMenuItem>Rust</DropdownMenuItem>
                <DropdownMenuItem>Astro</DropdownMenuItem>
                <DropdownMenuItem>Ethereum</DropdownMenuItem>
                <DropdownMenuItem>Web 3</DropdownMenuItem>
                <DropdownMenuItem>Web Assembly</DropdownMenuItem>
                <DropdownMenuItem>Framer Motion</DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          </ul>
        </div>

        <div className="flex flex-wrap justify-around gap-1">
          <ProjectCard
            title="Javascript Practice"
            desc="JavaScript is a lightweight interpreted programming language."
          />
          <ProjectCard
            title="Typescript Practice"
            desc="TypeScript is JavaScript with syntax for types. TypeScript is a strongly typed programming language that builds on JavaScript, giving you better tooling at any scale."
          />
          <ProjectCard
            title="Go Practice"
            desc="Go is a statically typed, compiled high-level programming language designed at Google"
          />
          <ProjectCard
            title="Next.js Practice"
            desc="Next.js is an open-source web development framework created by the private company Vercel providing React-based web applications with server-side rendering and static website generation."
          />
        </div>
      </div>
    </div>
  );
};

export default Skills;