import { ThemeProvider } from "@/components/theme-provider";
import Navvbar from './components/ui/Navvbar';
import About from './components/ui/About';
import Skills from './components/ui/Skills';
import Timeline from './components/ui/Timeline';
import Contact from './components/ui/Contact';
import ToTop from './components/ui/ToTop';
import GithubCalendar from './components/ui/GithubCalendar';
import { Toaster } from "@/components/ui/toaster";

export default function Home() {

  return (
    <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
      <div className="relative">
        <Navvbar />
        <About />
        <Skills />
        <GithubCalendar />
        <Timeline />
        <Contact />
        <Toaster />
        {/* <ToTop rocket={rocket} setRocket={setRocket}/> */}
        <ToTop />
      </div>
    </ThemeProvider>
  );
}